﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Isi2.Projekt.Common.Proxy.Service.Contracts
{
    [DataContract]
    public class PagedListRequestContract<T> where T : class
    {
        [DataMember]
        public IEnumerable<T> Items { get; set; }

        [DataMember]
        public bool Success { get; set; }

        [DataMember]
        public int Count { get; set; }
    }
}
